require('./models/User');
require('./models/Track');
const express = require('express');
const mongoose = require('mongoose');
const authRoutes = require('./routes/authRoutes');
const bodyParser = require('body-parser');
const trackRoutes = require('./routes/trackRoutes');
const requireAuth = require('./middlewares/requireAuth');

const app = express();

// Ensure bodyParser is called first
// this ensures that all the JSON objects are parsed
// first.
app.use(bodyParser.json());
app.use(authRoutes);
app.use(trackRoutes);

const mongourl = 'mongodb+srv://admin:adminadminPass@cluster0.rk355.mongodb.net/<dbname>?retryWrites=true&w=majority';

mongoose.connect(mongourl, {
    useNewUrlParser: true,
    useCreateIndex: true
});

mongoose.connection.on('connected', () => {
    console.log('Connected to mongo instance');
});

mongoose.connection.on('error', (err) => {
    console.error('Error connecting to mongo', err);
});

// Allows only authenticated users
// requireAuth first authorized users
app.get('/', requireAuth, (req, res) => {
    res.send(`Your email: ${req.user.email}`);
});

app.listen(3000, () => {
    console.log('Listening on port 3000');
});