const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const User = mongoose.model('User');

const router = express.Router();

router.post('/signup', async (req, res) => {
    const { email, password } = req.body;
    try{
        const user = new User({ email, password });
        await user.save();
        // Send back JWT (user token) after sign up
        const token = jwt.sign({ userID: user._id }, 'MY_SECRET_KEY');
        // User receives token and a custom message
        res.send({ token: token, message: 'You signed up successfully'});
    } catch(err){
        // HTTP status code 422
        return res.status(422).send(err.message);
    }
});

router.post('/signin', async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password){
        return res.status(422).send({ error: 'Must provide email and password' })
    }

    const user = await User.findOne( { email: email}); // async operation
    if (!user){
        return res.status(404).send({ error: 'Invalid password or email'});
    }

    try{
        await user.comparePassword(password);
        const token = jwt.sign({ userID: user._id}, 'MY_SECRET_KEY');
        res.send({ token: token, message: 'Welcome back!'});
    } catch(err){
        return res.status(422).send({ error: 'Invalid password or email'});
    }
});

module.exports = router;